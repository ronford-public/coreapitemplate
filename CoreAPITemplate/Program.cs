using Application.HttpServices.HttpClients;
using Application.Services.Abstractions;
using Application.Services.Implementations;
using Domain.Configurations;
using FluentValidation;
using FluentValidation.AspNetCore;
using Infrastructure.Extensions;
using Infrastructure.Shared.Extensions;
using MediatR;
using Microsoft.AspNetCore.Mvc.Formatters;
using CoreAPITemplate.ActionFilters;
using CoreAPITemplate.Extensions;
using CoreAPITemplate.Middlewares;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddFluentValidationAutoValidation().AddFluentValidationClientsideAdapters();
builder.Services.AddValidatorsFromAssemblyContaining<Domain.Assembly.AssemblyReference>();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.ConfigureSwagger();
builder.Services.AddHttpContextAccessor();
builder.Services.AddCorsConfiguration();
builder.Services.AddSharedInfrastructure();
builder.Services.AddDatabaseContext(builder.Configuration);
builder.Services.AddRepositories();
builder.Services.AddMappingProfile();
builder.Services.ConfigureLoggingProviders();
builder.Services.AddTransient<IRestClient, JsonRestClient>();
builder.Services.AddScoped<ITestService, TestService>();
builder.Services.AddHttpClient<ITestService, TestService>();

builder.Services.AddLazyCache();
builder.Services.AddScoped<IpCheckActionFilter>();

builder.Services.AddMediatR(typeof(Application.Assembly.AssemblyReference).Assembly);

builder.Services.AddControllers(config =>
{
    config.RespectBrowserAcceptHeader = true;
    config.ReturnHttpNotAcceptable = true;

    // remove string output formatter. default json formatter will be used
    config.OutputFormatters.RemoveType<StringOutputFormatter>();
    // config.OutputFormatters.RemoveType<HttpNoContentOutputFormatter>();
}).AddNewtonsoftJson(o =>
{
    o.SerializerSettings.ContractResolver = new DefaultContractResolver
    {
        NamingStrategy = new CamelCaseNamingStrategy()
    };
    o.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
});

builder.Host.AddSerilogLogger();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
}

app.UseSwagger();

app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "CoreAPITemplate v1");
        c.SwaggerEndpoint("/swagger/v2/swagger.json", "CoreAPITemplate v2");
    }
);
app.UseMiddleware<ExceptionMiddleware>();

app.UseCors("CorsPolicy");

if (bool.TryParse(builder.Configuration[$"{RuntimeSystemConfiguration.SectionName}:ForceHttpsRedirect"],
        out var forceHttpsRedirect))
{
    if (forceHttpsRedirect)
    {
        app.UseHttpsRedirection();
    }
}

app.UseAuthorization();

app.MapControllers();

app.MigrateDatabase();

app.Lifetime.ApplicationStarted.Register(OnStarted);
app.Lifetime.ApplicationStopping.Register(OnApplicationShutdown);

app.Run();

void OnStarted()
{
    Console.WriteLine("Application started");
}

void OnApplicationShutdown()
{
    Log.Information("Application shutting down");
    Log.CloseAndFlush();
    //Wait while the data is flushed
    Thread.Sleep(2000);
}