using System.Net.Mime;
using Application.Features.Test.Commands;
using CoreAPITemplate.ActionFilters;
using Domain.Boundary.Requests;
using Infrastructure.Shared.Wrapper;
using Microsoft.AspNetCore.Mvc;

namespace CoreAPITemplate.Controllers.Test.v1
{
    [Route("api/v1/Test")]
    [ServiceFilter(typeof(IpCheckActionFilter))]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiExplorerSettings(GroupName = "v1")]
    [ProducesResponseType(typeof(Result<string>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(Result<string>), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(Result<string>), StatusCodes.Status500InternalServerError)]
    public class TestController : BaseApiController<TestController>
    {
        [HttpPost("SaveApiMethod")]
        public async Task<IActionResult> SaveApiPostRequest([FromBody] ApiPostMethodRequest request)
        {
            var response = await _mediatr.Send(new SaveApiRequestCommand(request));
            return Ok(response);
        }
    }
}