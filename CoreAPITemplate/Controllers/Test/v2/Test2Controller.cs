using System.Net.Mime;
using Application.Features.Test.Commands;
using CoreAPITemplate.ActionFilters;
using Domain.Boundary.Requests;
using Domain.Boundary.Responses;
using Infrastructure.Shared.Wrapper;
using Microsoft.AspNetCore.Mvc;

namespace CoreAPITemplate.Controllers.Test.v2
{
    [Route("api/v2/Test")]
    [ServiceFilter(typeof(IpCheckActionFilter))]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiExplorerSettings(GroupName = "v2")]
    [ProducesResponseType(typeof(Result<string>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(Result<string>), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(Result<string>), StatusCodes.Status500InternalServerError)]
    public class Test2Controller : BaseApiController<Test2Controller>
    {
        [HttpPost("TestService")]
        public async Task<ActionResult<ServiceTestResponse>> TestServiceRequest([FromBody] ServiceTestRequest request)
        {
            var response = await _mediatr.Send(new TestServiceCommand(request));
            return Ok(response);
        }
    }
}