﻿using Infrastructure.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Reflection;

namespace CoreAPITemplate.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDatabaseContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            var enableLogging = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";

            services.AddDbContext<DatabaseContext>(
                dbContextOptions => dbContextOptions
                    .UseSqlServer(connectionString, options =>
                    {
                        options.MigrationsAssembly("Infrastructure");
                    })
                    .EnableSensitiveDataLogging(enableLogging)
                    .EnableDetailedErrors(enableLogging)
            );

            return services;
        }

        public static WebApplication MigrateDatabase(this WebApplication webApplication)
        {
            using var scope = webApplication.Services.CreateScope();
            using var appContext = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
            if (appContext.Database.GetPendingMigrations().Any())
            {
                appContext.Database.Migrate();
            }

            return webApplication;
        }

        public static void AddCorsConfiguration(this IServiceCollection services) =>
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder =>
                    builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            });

        public static void ConfigureSwagger(this IServiceCollection services) =>

         services.AddSwaggerGen(c =>
         {
             c.SwaggerDoc("v1", new OpenApiInfo
             {
                 Title = "Core APi Template",
                 Version = "v1",
                 Description = "CoreAPITemplate",
                 Contact = new OpenApiContact
                 {
                     Name = "Ronford Digital",
                     Url = new Uri("https://ronforddigital.com")
                 }
             });
             
             c.SwaggerDoc("v2", new OpenApiInfo
             {
                 Title = "Core Api Template",
                 Version = "v2",
                 Description = "CoreAPITemplate",
                 Contact = new OpenApiContact
                 {
                     Name = "Ronford Digital",
                     Url = new Uri("https://ronforddigital.com")
                 }
             });
             c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
             // Set the comments path for the Swagger JSON and UI.
             var xmlFile = $"CoreAPITemplate.{Assembly.GetExecutingAssembly().GetName().Name}.xml";
             var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
             if (File.Exists(xmlPath))
                 c.IncludeXmlComments(xmlPath);
         });

        public static void ConfigureLoggingProviders(this IServiceCollection services)
        {
            // https://weblog.west-wind.com/posts/2018/Dec/31/Dont-let-ASPNET-Core-Default-Console-Logging-Slow-your-App-down
            services.AddLogging(config =>
            {
                // clear out default configuration. Providers will be added by serilog
                config.ClearProviders();

                if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == Environments.Development)
                {
                    /*config.AddDebug();
                    config.AddEventSourceLogger();
                    config.AddConsole();*/
                }
            });
        }
    }
}