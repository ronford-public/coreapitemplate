﻿using Serilog;
using Serilog.Exceptions;
using Serilog.Sinks.Elasticsearch;
using System.Net;
using ILogger = Serilog.ILogger;

namespace CoreAPITemplate.Extensions
{
    internal static class HostBuilderExtensions
    {
        internal static IHostBuilder AddSerilogLogger(this IHostBuilder builder)
        {
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? Environments.Development;
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile(path: $"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();
            
            Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();
            SerilogHostBuilderExtensions.UseSerilog(builder);       
            
            return builder;
        }

    }
}