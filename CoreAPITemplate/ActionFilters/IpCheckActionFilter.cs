﻿using Domain.Configurations;
using Domain.Constants;
using Infrastructure.Shared.Wrapper;
using LazyCache;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using System.Net;

namespace CoreAPITemplate.ActionFilters
{
    public class IpCheckActionFilter : ActionFilterAttribute
    {
        private readonly ILogger<IpCheckActionFilter> _logger;

        private readonly SafeListConfiguration _safeListConfig;
        private readonly IAppCache _lazyCache;

        public IpCheckActionFilter(ILogger<IpCheckActionFilter> logger, IOptions<SafeListConfiguration> safeListConfig, IAppCache lazyCache)
        {
            _logger = logger;

            _safeListConfig = safeListConfig.Value;
            _lazyCache = lazyCache;
        }

        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (_safeListConfig.SafeListEnabled)
            {
                var remoteIp = context.HttpContext.Connection.RemoteIpAddress;
                var validIps = await _lazyCache.GetOrAddAsync(CacheConstants.GetTrustedIpsCacheKey, GetTrustedIpsAsync, DateTimeOffset.Now.AddMinutes(10));

                var badIp = true;

                if (remoteIp is { IsIPv4MappedToIPv6: true })
                {
                    remoteIp = remoteIp.MapToIPv4();
                }

                foreach (var address in validIps)
                {
                    IPAddress.TryParse(address, out var testIp);

                    if (testIp != null && testIp.Equals(remoteIp))
                    {
                        badIp = false;
                        break;
                    }
                }

                if (badIp)
                {
                    _logger.LogWarning("Forbidden Request from IP: {RemoteIp}", remoteIp);
                    var response = await Result.FailAsync($"Your IP {remoteIp} is not authorized",
                      HttpStatusCode.Unauthorized);
                    context.Result = new UnauthorizedObjectResult(response);
                    return;
                }
            }
            await base.OnActionExecutionAsync(context, next);
        }

        private async Task<List<string>> GetTrustedIpsAsync()
        {

            var validIps = new List<string>();

            if (validIps.Count == 0)
            {
                validIps = _safeListConfig.DefaultIpSafeList.Split(',').Select(ip => ip.Trim()).ToList();
            }

            return await Task.FromResult(validIps);
        }

    }
}
