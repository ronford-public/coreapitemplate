﻿using Domain.Boundary.Requests;
using Domain.Entities;

namespace Infrastructure.Factories;

public static class RequestToEntityFactory
{
    public static ApiPostMethod ToApiPostMethodEntity(this ApiPostMethodRequest request)
    {
        return new ApiPostMethod
        {
            Id = request.Id,
            Method = request.Method,
            Url = request.Url
        };
    }
}