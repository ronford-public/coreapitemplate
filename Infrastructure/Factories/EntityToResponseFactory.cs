﻿using Domain.Boundary.Responses;
using Domain.Entities;

namespace Infrastructure.Factories
{
    public static class EntityToResponseFactory
    {
        public static ApiPostMethodResponse ToApiPostMethodResponse(this ApiPostMethod request)
        {
            return new ApiPostMethodResponse
            {
                Status = new StatusResponse
                {
                    StatusCode = 200,
                    StatusName = "Success",
                    StatusDescription = "Request was successfully"
                },
                Data = new ApiResponseData
                {
                    Id = request.Id,
                    Method = request.Method,
                    Url = request.Url,
                    CreatedOn = request.CreatedOn,
                    LastModifiedOn = request.LastModifiedOn
                }
            };
        }
    }
}