<h2>Usage</h2>

<ul>
<li>Clone the template in a project/folder called <b>CoreAPITemplate</b>.</li>
<li>Navigate to the root directory of the cloned template.</li>
<li>Run <code>dotnet new -i .</code> in your preferred command/terminal to install the template as a nuget package.</li>
<li>Navigate to a directory where you want to create your project.</li>
<li>Run <code>dotnet new coreapi -n <YourProjectName>{YourProjectName}</YourProjectName> --postgres false/true --elastic true/false --logging {elastic-logging-index}</code>:
<h5>Database</h5>
<ul>
    <li>pass <code>true</code> if you want to use <b>postgres</b> as your db</li>
    <li>pass <code>false</code> if you want to use <b>sqlserver</b> as your db</li>
</ul>

<h5>Logging</h5>
<ul>
    <li>pass <code>true</code> if you want to use <b>elastic search</b> for logging</li>
    <li>pass <code>false</code> if you want to use <b>serilog(file logging)</b> for logging</li>
</ul>

<h5>Logging Index</h5>
<ul>
    <li>Replace <code>elastic-logging-index</code> with your preferred logging index</li>
</ul>

</li>
</ul>
