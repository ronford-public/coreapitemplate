﻿namespace Domain.Constants
{
    public static class CacheConstants
    {
        public const string GetTrustedIpsCacheKey = "trusted-ips";
    }
}
