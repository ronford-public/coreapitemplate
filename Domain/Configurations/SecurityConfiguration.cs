﻿using System.Security.Cryptography.X509Certificates;

namespace Domain.Configurations
{
    public class SecurityConfiguration
    {
        public const string SectionName = "SecurityConfiguration";
        public string CertificatePassword { get; set; }
        public string ApiKey { get; set; }
        public X509Certificate2Collection CertificateCollection { get; set; }
    }
}