﻿namespace Domain.Configurations
{
    public class SafeListConfiguration
    {
        public const string SectionName = "SafeListConfiguration";
        public string DefaultIpSafeList { get; set; }
        public bool SafeListEnabled { get; set; }
    }
}
