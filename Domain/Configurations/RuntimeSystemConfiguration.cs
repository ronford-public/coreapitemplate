﻿namespace Domain.Configurations
{
    public class RuntimeSystemConfiguration
    {
        public const string SectionName = "RuntimeSystemConfiguration";
        public bool UseFakePaymentServices { get; set; }
        public bool ForceHttpsRedirect { get; set; }
        public bool AllowTestEncryptionEndpoints { get; set; }
    }
}
