﻿using Domain.Boundary.Requests;

namespace Domain.Boundary.Responses;

public class ServiceTestData
{
    public string Name { get; set; }
    public  int Age { get; set; }
    public int Count { get; set; }
}