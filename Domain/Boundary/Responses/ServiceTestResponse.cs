﻿namespace Domain.Boundary.Responses;

public class ServiceTestResponse
{
    public StatusResponse Status { get; set; }
    public ServiceTestData Data { get; set; }
}