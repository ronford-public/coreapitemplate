﻿namespace Domain.Boundary.Responses;

public class ApiPostMethodResponse
{
    public StatusResponse Status{get;set;}
    public ApiResponseData Data{get;set;}
}