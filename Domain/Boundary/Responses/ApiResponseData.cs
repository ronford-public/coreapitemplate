﻿namespace Domain.Boundary.Responses;

public class ApiResponseData
{
    public int Id { get; set; }
    public string Method { get; set; }
    public string Url { get; set; }
    public DateTimeOffset CreatedOn { get; set; }
    public DateTimeOffset? LastModifiedOn { get; set; }
}