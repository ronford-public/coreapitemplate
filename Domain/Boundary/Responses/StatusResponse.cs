﻿namespace Domain.Boundary.Responses;

public class StatusResponse
{
    public int StatusCode{get;set;}
    public string StatusName{get;set;}
    public string StatusDescription{get;set;}
    public bool IsSuccess { get; set; }
}