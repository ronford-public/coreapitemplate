﻿namespace Domain.Boundary.Requests;

public class ApiPostMethodRequest
{
    public int Id { get; set; }
    public string Method { get; set; }
    public string Url { get; set; }
}