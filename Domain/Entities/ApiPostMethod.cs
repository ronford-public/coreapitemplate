﻿using Domain.Contracts;

namespace Domain.Entities;

public class ApiPostMethod : BaseEntity
{
    public int Id { get; set; }
    public string Method { get; set; }
    public string Url { get; set; }
}