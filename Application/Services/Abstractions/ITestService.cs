﻿using Domain.Boundary.Requests;
using Domain.Boundary.Responses;
using Infrastructure.Shared.Wrapper;

namespace Application.Services.Abstractions;

public interface ITestService
{
    Task<IResult<ServiceTestData>> TestServiceRequest(ServiceTestRequest request);
}