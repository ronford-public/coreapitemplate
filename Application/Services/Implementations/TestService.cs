﻿using System.Text;
using Application.Services.Abstractions;
using Domain.Boundary.Requests;
using Domain.Boundary.Responses;
using Infrastructure.Shared.Extensions;
using Infrastructure.Shared.Helpers;
using Infrastructure.Shared.Wrapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Application.Services.Implementations;

public class TestService : ITestService
{
    private readonly HttpClient _httpClient;

    public TestService(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }
    public async Task<IResult<ServiceTestData>>  TestServiceRequest(ServiceTestRequest request)
    {
        var serializerSettings = SerializationSettingsExtensions.GetSettings(new SnakeCaseNamingStrategy());
        var stringContent =
            new StringContent(JsonConvert.SerializeObject(request, serializerSettings), Encoding.UTF8,
                "application/json");
        var httpResponseMessage = await _httpClient.GetAsync($"https://api.agify.io/?name={request.Name}");


        var response =
            await httpResponseMessage
                .HandleJsonResponse<ServiceTestData, ServiceTestData>(
                    serializerSettings, "Failed to post data");

        return await Result<ServiceTestData>.SuccessAsync(response, "Failed to send request");
    }
}