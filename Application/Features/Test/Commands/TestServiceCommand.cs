﻿using Application.Services.Abstractions;
using Domain.Boundary.Requests;
using Domain.Boundary.Responses;
using MediatR;

namespace Application.Features.Test.Commands;

public class TestServiceCommand : IRequest<ServiceTestResponse>
{
    public ServiceTestRequest Request { get; set; }

    public TestServiceCommand(ServiceTestRequest request)
    {
        Request = request;
    }
}

internal class PostTestServiceCommandHandler : IRequestHandler<TestServiceCommand, ServiceTestResponse>
{
    private readonly ITestService _testService;

    public PostTestServiceCommandHandler(ITestService testService)
    {
        _testService = testService;
    }
    public async Task<ServiceTestResponse> Handle(TestServiceCommand request, CancellationToken cancellationToken)
    {
        var response = await _testService.TestServiceRequest(request.Request);
        if (response.Succeeded)
        {
            return new ServiceTestResponse
            {
                Status = new StatusResponse
                {
                    StatusCode = 200,
                    StatusName = "Success",
                    StatusDescription = "Request was successfully",
                    IsSuccess = true
                },
                Data = new ServiceTestData
                {
                    Name = response.Data.Name,
                    Age = response.Data.Age,
                    Count = response.Data.Count
                }
            }; 
        }
        
        return new ServiceTestResponse
        {
            Status = new StatusResponse
            {
                StatusCode = 200,
                StatusName = "Failure",
                StatusDescription = "Request failed",
                IsSuccess = false
            },
            Data = null
        }; 
    }
}