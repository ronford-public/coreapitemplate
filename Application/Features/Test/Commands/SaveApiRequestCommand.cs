﻿using Domain.Boundary.Requests;
using Domain.Boundary.Responses;
using Domain.Entities;
using Infrastructure.Factories;
using Infrastructure.Repository.Abstractions;
using Infrastructure.Shared.Extensions;
using Infrastructure.Shared.Wrapper;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Features.Test.Commands;

public class SaveApiRequestCommand : IRequest<ApiPostMethodResponse>
{
    public ApiPostMethodRequest PostMethodRequest { get; set; }

    public SaveApiRequestCommand(ApiPostMethodRequest postMethodRequest)
    {
        PostMethodRequest = postMethodRequest;
    }
}

internal class SaveApiRequestCommandHandler : IRequestHandler<SaveApiRequestCommand, ApiPostMethodResponse>
{
    private readonly IRepositoryUnit _repository;

    public SaveApiRequestCommandHandler(IRepositoryUnit repository)
    {
        _repository = repository;
    }
    public async Task<ApiPostMethodResponse> Handle(SaveApiRequestCommand request, CancellationToken cancellationToken)
    {
        ApiPostMethod apiRequest;
        if (request.PostMethodRequest.Id == 0)
        {
            apiRequest = request.PostMethodRequest.ToApiPostMethodEntity();
            _repository.DbContext().ApiPostMethods.Add(apiRequest);
        }
        else
        {
            apiRequest = await _repository.Entity<ApiPostMethod>()
                .Where(am => am.Id == request.PostMethodRequest.Id)
                .SingleOrDefaultAsync(cancellationToken);
            if (apiRequest is null)
            {
                return new ApiPostMethodResponse
                {
                    Status = new StatusResponse
                    {
                        StatusCode = 200,
                        StatusName = "Failure",
                        StatusDescription = "Request Failed",
                        IsSuccess = false
                    },
                    Data = new ApiResponseData
                    {
                        Id = request.PostMethodRequest.Id,
                        Method = request.PostMethodRequest.Method,
                        Url = request.PostMethodRequest.Url
                    }
                };
            }
            
            apiRequest.Method = request.PostMethodRequest.Method;
            apiRequest.Url = request.PostMethodRequest.Url;
            _repository.DbContext().ApiPostMethods.Update(apiRequest);
        }
       
        await _repository.SaveAsync("Failed to save data", cancellationToken);
        var response = apiRequest.ToApiPostMethodResponse();

        return response;
    }
}