﻿namespace Infrastructure.Shared.Extensions
{
    public static class DecimalExtensions
    {
        public static decimal Round(this decimal value, int precision = 2)
        {
            return decimal.Round(value, precision, MidpointRounding.AwayFromZero);
        }
    }
}