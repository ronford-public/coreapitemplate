﻿using Infrastructure.Shared.CustomExceptions;
using Infrastructure.Shared.Wrapper;
using Newtonsoft.Json;
using System.Net.Mime;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Infrastructure.Shared.Extensions
{
    public static class HttpResponseExtensions
    {
        public static async Task<TResult> HandleJsonResponse<TResult, TError>(this HttpResponseMessage httpResponse,
            JsonSerializerSettings serializerSettings, string errorMessage)
        {
            if (!httpResponse.IsSuccessStatusCode)
            {
                await httpResponse.HandleHttpError<TError>(serializerSettings, errorMessage).ConfigureAwait(false);
            }

            if (httpResponse.Content.Headers.ContentType?.MediaType != MediaTypeNames.Application.Json) return default;

            var content = await httpResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
            return JsonConvert.DeserializeObject<TResult>(content, serializerSettings);
        }

        public static async Task HandleHttpError<T>(this HttpResponseMessage httpResponse,
            JsonSerializerSettings serializerSettings, string defaultErrorMessage)
        {
            T error;

            if (httpResponse.Content.Headers.ContentType?.MediaType == "text/xml")
            {
                var serializer = new XmlSerializer(typeof(T), new XmlRootAttribute("Response"));
                using var reader =
                    new StringReader(await httpResponse.Content.ReadAsStringAsync().ConfigureAwait(false));
                error = (T)serializer.Deserialize(reader);
                throw new ApiException(defaultErrorMessage, httpResponse.StatusCode);
            }

            error = JsonConvert.DeserializeObject<T>(await httpResponse.Content.ReadAsStringAsync(),
                serializerSettings);

            if (error == null)
            {
                throw new ApiException(defaultErrorMessage, httpResponse.StatusCode);
            }

            throw new ApiException(JsonConvert.SerializeObject(error), httpResponse.StatusCode);
        }

        public static async Task ThrowResponseExceptionAsync(this HttpResponseMessage httpResponse, string defaultErrorMessage)
        {
            var err = JsonConvert.DeserializeObject<Result<string>>(await httpResponse.Content.ReadAsStringAsync());
            if (string.IsNullOrEmpty(err?.Messages[0]))
            {
                throw new ApiException(defaultErrorMessage, httpResponse.StatusCode);
            }
            throw new ApiException(err?.Messages[0], httpResponse.StatusCode);
        }
    }
}