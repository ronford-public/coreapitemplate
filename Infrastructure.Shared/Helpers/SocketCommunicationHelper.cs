﻿using System.Net;
using System.Net.Sockets;
using System.Text;
using Microsoft.Extensions.Logging;

namespace Infrastructure.Shared.Helpers
{
    public class SocketCommunicationHelper
    {
        public static string PostSocketMessage(string ip, int port, string message, ILogger logger)
        {
            var responseData = string.Empty;
            byte[] bytes = new byte[4096];
            try
            {
                IPAddress ipAddress = IPAddress.Parse(ip);
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

                // Create a TCP/IP  socket.
                Socket sender = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                // Connect the socket to the remote endpoint. Catch any errors.
                try
                {
                    sender.Connect(remoteEP);

                    logger.LogInformation("Socket connected to {0}",
                        sender.RemoteEndPoint.ToString());

                    // Encode the data string into a byte array.
                    byte[] msg = Encoding.ASCII.GetBytes(message);

                    // Send the data through the socket.
                    int bytesSent = sender.Send(msg);

                    // Receive the response from the remote device.
                    int bytesRec = sender.Receive(bytes);
                    responseData = Encoding.ASCII.GetString(bytes, 0, bytesRec);

                    logger.LogInformation("Echoed test = {0}", responseData);

                    // Release the socket.
                    sender.Shutdown(SocketShutdown.Both);
                    sender.Close();
                }
                catch (ArgumentNullException ane)
                {
                    logger.LogError("ArgumentNullException : {0}", ane.ToString());
                }
                catch (SocketException se)
                {
                    logger.LogError("SocketException : {0}", se.ToString());
                }
                catch (Exception e)
                {
                    logger.LogError("Unexpected exception : {0}", e.ToString());
                }
            }
            catch (Exception e)
            {
                logger.LogError(e.ToString());
            }

            return responseData;
        }
    }
}