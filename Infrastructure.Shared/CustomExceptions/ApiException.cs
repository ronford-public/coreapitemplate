using System.Net;
using Infrastructure.Shared.CustomExceptions.Models;

namespace Infrastructure.Shared.CustomExceptions
{
    public class ApiException : Exception
    {
        public ApiException()
        {
            Errors = Enumerable.Empty<ValidationError>();
        }

        public HttpStatusCode StatusCode { get; set; }

        public IEnumerable<ValidationError> Errors { get; set; }

        public ApiException(string message, HttpStatusCode statusCode = HttpStatusCode.InternalServerError) :
            base(message)
        {
            StatusCode = statusCode;
        }

        public ApiException(string message,
            IEnumerable<ValidationError> errors,
            HttpStatusCode statusCode = HttpStatusCode.InternalServerError) :
            base(message)
        {
            StatusCode = statusCode;
            Errors = errors;
        }
    }
}